
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.ServerSocket;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;

public class Server {

    public static void main(String[] args) throws IOException {

        String imageName = "screenshot";
        int imageCount = 1;

        ServerSocket server = new ServerSocket(7777);

        while (true) {

            System.out.println("Esperando imagem.. ");

            Socket socket = server.accept();

            //Captura imagem
            BufferedImage image = ImageIO.read(ImageIO.createImageInputStream(socket.getInputStream()));
            if (image != null) {
                System.out.println("Lendo imagem.. ");
                //Printa tamanho da imagem recebida 
                System.out.println("Recebido:" + image.getHeight() + "x" + image.getWidth() + ": " + System.currentTimeMillis());
                //Salva imagem                                               
                ImageIO.write(image, "jpg", new File("/home/luan/Pictures/" + imageName + Integer.toString(imageCount) + ".png"));
                imageCount++;

            } else {
                System.out.println("Não recebeu imagem");
            }

            socket.close();

        }

    }
}
