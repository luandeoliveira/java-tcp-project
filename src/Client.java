import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.List;
import javax.imageio.ImageIO;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) throws IOException, InterruptedException, UnsupportedFlavorException {

        boolean proceed = true;

        try {
           

            do {
                Socket socket = new Socket("localhost", 7777);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

                if (clipboard.isDataFlavorAvailable(DataFlavor.imageFlavor)) {
                    //Pegando imagem da área de transferência                        
                    BufferedImage image = (BufferedImage) clipboard.getData(DataFlavor.imageFlavor);

                    OutputStream outputStream = socket.getOutputStream();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                    ImageIO.write(image, "jpg", byteArrayOutputStream);

                    ByteBuffer byteBuffer = ByteBuffer.allocate(4).putInt(byteArrayOutputStream.size());
                    byte[] size = new byte[byteBuffer.remaining()];
                    outputStream.write(size);
                    outputStream.write(byteArrayOutputStream.toByteArray());
                    //Enviando dados
                    outputStream.flush();

                    System.out.println("Imagem enviada");

                    Thread.sleep(5000);
                    System.out.println("Fechando.. " + System.currentTimeMillis());
                } else {
                    System.out.println("O conteúdo na área de transferência não é uma imagem");
                }
                
                socket.close();

                Scanner scanner = new Scanner(System.in);
                System.out.println("Deseja enviar outra imagem?");
                String answer = scanner.nextLine();

                if (!answer.equals("sim")) {
                    proceed = false;
                }
                 

            } while (proceed == true);

           

        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
